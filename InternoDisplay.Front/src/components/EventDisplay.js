import PropTypes from 'prop-types';
import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";
import { Button, Input, notification } from "antd";
import React, { useEffect, useState } from "react";

const EventDisplay = () => {
    const [connection, setConnection] = useState(null);
    const [event, setEvent] = useState("SD");
    const [bg, setBG] = useState("#73ba76");
    useEffect(() => {
      const connect = new HubConnectionBuilder()
        .withUrl("/seed")
        .withAutomaticReconnect()
        .build();
      setConnection(connect);
    }, []);
  
    useEffect(() => {
      if (connection) {

        connection
          .start()
          .then(() => {
            connection.on("EventRecieved", (event) => {
              console.log(event);
              setEvent(event);

              if(event == 'AV')
                setBG('#de5253')
              else
                setBG('#73ba76')
            });
          })
          .catch((error) => console.log(error));
      }
    }, [connection]);
  
  
    return (
      <div style={{display:'flex',alignContent:'center', alignItems:'center',justifyContent:'center',height:'700px',background:bg}}>
        <img width={'500px'} src={'images-eventos/' + event + '.jpg'} />
      </div>
    );
};

EventDisplay.propTypes = {};

export default EventDisplay;