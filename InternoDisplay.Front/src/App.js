import logo from './logo.svg';
import './App.css';
import EventDisplay from './components/EventDisplay';

function App() {
  return <EventDisplay />;
}

export default App;
