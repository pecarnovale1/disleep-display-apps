import PropTypes from 'prop-types';
import { HubConnection, HubConnectionBuilder } from "@microsoft/signalr";
import { Button, Input, notification } from "antd";
import React, { useEffect, useState } from "react";

const SignalDisplay = () => {
    const [connection, setConnection] = useState(null);
    const [signal, setSignal] = useState("No-Detection");
  
    useEffect(() => {
      const connect = new HubConnectionBuilder()
        .withUrl("/seed")
        .withAutomaticReconnect()
        .build();
      setConnection(connect);
    }, []);
  
    useEffect(() => {
      if (connection) {

        connection
          .start()
          .then(() => {
            connection.on("SignalRecieved", (signal) => {
              setSignal(signal);
            });
          })
          .catch((error) => console.log(error));
      }
    }, [connection]);
  
  
    return (
      <div style={{display:'flex',alignContent:'center', alignItems:'center',justifyContent:'center',height:'500px',background:'black'}}>
        <img width={'150px'} height={'150px'} src={'images/' + signal + '.png'} />
      </div>
    );
};

SignalDisplay.propTypes = {};

export default SignalDisplay;