import logo from './logo.svg';
import './App.css';
import SignalDisplay from './components/SignalDisplay';

function App() {
  return <SignalDisplay />;
}

export default App;
