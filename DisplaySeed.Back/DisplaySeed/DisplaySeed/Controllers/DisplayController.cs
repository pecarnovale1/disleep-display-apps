﻿using DisplaySeed.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace DisplaySeed.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DisplayController : ControllerBase
    {
        private IHubContext<Hubs> _hubContext{ get; set; }

        public DisplayController(IHubContext<Hubs> hubcontext)
        {
            _hubContext = hubcontext;
        }
        [HttpPost("SendSignal")]
        public async Task<bool> SendSignal(string signal)
        {
            await _hubContext.Clients.All.SendAsync("SignalRecieved", signal);

            return true;
        }

        [HttpPost("SendEvent")]
        public async Task<bool> SendEvent(string even)
        {
            await _hubContext.Clients.All.SendAsync("EventRecieved", even);

            return true;
        }
    }
}
