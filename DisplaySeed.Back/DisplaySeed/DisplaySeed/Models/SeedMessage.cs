﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace DisplaySeed.Models
{
    public class SeedMessage
    {
        public string Message { get; set; }
    }
}
