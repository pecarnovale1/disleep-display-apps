﻿using Microsoft.AspNetCore.SignalR;
using System.Threading.Tasks;

namespace DisplaySeed.Models
{
    public class Hubs : Hub
    {
        public async Task SendSignal(string signal)
        {
            await Clients.All.SendAsync("SignalRecieved", signal);
        }

        public async Task SendEvent(string even)
        {
            await Clients.All.SendAsync("EventRecieved", even);
        }
    }
} 
